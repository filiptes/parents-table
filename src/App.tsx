import { ParentsTable } from "./components/parentsTable/ParentsTable";

function App() {
  return (
    <div style={{ padding: 32, maxWidth: 1100, margin: "0 auto" }}>
      <ParentsTable />
    </div>
  );
}

export default App;
