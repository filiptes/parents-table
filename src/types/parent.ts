export interface Parent {
  id: string;
  firstName: string;
  lastName: string;
}
