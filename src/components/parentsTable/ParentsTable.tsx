import debounce from "lodash/debounce";
import { ChangeEvent, useRef, useState } from "react";
import { search } from "../../utils/search";
import { parents } from "./ParentsTable.utils";
import { ParentsTableRow } from "./row/ParentsTableRow";

export function ParentsTable() {
  const [inputValue, setInputValue] = useState("");
  const [displayedData, setDisplayedData] = useState(parents);

  const debouncedSearch = useRef(
    debounce((str: string) => {
      const filteredData = search(parents, str);
      setDisplayedData(filteredData);
    }, 100)
  ).current;

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    debouncedSearch(event.target.value);
  };

  return (
    <div>
      <div style={{ marginBottom: 16 }}>
        <input value={inputValue} onChange={handleInputChange} />
      </div>

      <table border={2}>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>

        <tbody>
          {displayedData.map((item) => (
            <ParentsTableRow key={item.id} {...item} />
          ))}

          {displayedData.length === 0 && <p>No data</p>}
        </tbody>
      </table>
    </div>
  );
}
