import { Parent } from "../../../types/parent";

export function ParentsTableRow(props: Parent) {
  return (
    <tr>
      <td>{props.firstName}</td>
      <td>{props.lastName}</td>
    </tr>
  );
}
