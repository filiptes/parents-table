import { Parent } from "../types/parent";
import { normalizeString } from "./normalizeString";

export const search = (data: Parent[], str: string) => {
  const parts = normalizeString(str).trim().split(" ");

  if (parts[0] === "") {
    //if string is empty return all data
    return data;
  } else if (parts.length > 2) {
    //if string contains more than 2 words return empty array
    return [];
  } else if (parts.length === 1) {
    //if string contains 1 word search if it is included in first or last name
    return data.filter(
      (item) =>
        normalizeString(item.firstName).includes(parts[0]) ||
        normalizeString(item.lastName).includes(parts[0])
    );
  } else {
    //if string contains 2 words search if one of the words are included in first or last name
    return data.filter(
      (item) =>
        (normalizeString(item.firstName).includes(parts[0]) &&
          normalizeString(item.lastName).includes(parts[1])) ||
        (normalizeString(item.firstName).includes(parts[1]) &&
          normalizeString(item.lastName).includes(parts[0]))
    );
  }
};
